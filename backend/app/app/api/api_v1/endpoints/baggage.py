from typing import Any, List, Dict

from fastapi import APIRouter
from fastapi.responses import JSONResponse

from pydantic import BaseModel

from app.exceptions import WrongStatusCode

import httpx

from app.core.config import settings
from app.schemas.cart import ShoppingCart

from app.schemas.pricing import AncillariesPricings

router = APIRouter()


class Body(BaseModel):
    order_id: str
    surname: str


class ShoppingCartResponse(BaseModel):
    shoppingCart: ShoppingCart


def combine_ancillaries(ancillaries_pricings: AncillariesPricings) -> List[Dict]:
    """
    Combine passengers and route ids with baggage ids
    """
    result = []
    for ancillary_pricing in ancillaries_pricings:

        for another in ancillary_pricing.baggagePricings:
            filtered = list(map(lambda x: x.id, filter(lambda x: x.equipmentType == 'ski', another.baggages)))

            result += [{
                'passengerId': pid,
                'routeId': another.routeId,
                'baggageIds': filtered
            } for pid in another.passengerIds]

    return result


@router.post("/baggage-management/skis/", response_model=ShoppingCart, status_code=200)
async def baggage_mgmt_skis(
        body: Body
) -> Any:
    async with httpx.AsyncClient() as client:
        response = await client.get(settings.API_HOST + '/orders/', params=body.dict())

    if response.status_code != 200:
        raise WrongStatusCode

    ancillaries = AncillariesPricings.parse_obj(response.json())

    result = combine_ancillaries(ancillaries.ancillariesPricings)

    async with httpx.AsyncClient() as client:
        response = await client.put(settings.API_HOST + '/bags/', data={'baggageSelections': result})

    if response.status_code != 200:
        raise WrongStatusCode

    # Pass error back
    if 'error' in response.json():
        return JSONResponse(status_code=200, content={
            'error': response.json()['error']
        })

    shopping_cart = ShoppingCartResponse.parse_obj(response.json())

    return shopping_cart.dict()
