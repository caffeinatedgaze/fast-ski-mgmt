from typing import List, Optional

from pydantic.main import BaseModel


class Item(BaseModel):
    item_id: int


class ShoppingCart(BaseModel):
    items: Optional[List[Item]]
