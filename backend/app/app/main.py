from json.decoder import JSONDecodeError

from fastapi import FastAPI
from pydantic.error_wrappers import ValidationError
from starlette.middleware.cors import CORSMiddleware

from app.api.api_v1.api import api_router
from app.core.config import settings

from app.exceptions import WrongStatusCode
from app.handlers import validation_exception_handler, service_failed_exception_handler

app = FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f"{settings.API_V1_STR}/openapi.json"
)

# Assign handlers
app.add_exception_handler(ValidationError, handler=validation_exception_handler)
app.add_exception_handler(JSONDecodeError, handler=validation_exception_handler)
app.add_exception_handler(WrongStatusCode, handler=service_failed_exception_handler)

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_router, prefix=settings.API_V1_STR)
