from fastapi.encoders import jsonable_encoder
from pydantic.main import BaseModel
from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse


class Error(BaseModel):
    code: str
    message: str


class ErrorWrapper(BaseModel):
    error: Error


async def validation_exception_handler(request: Request, exc: Exception):
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({
            'error': {'code': 'external_service',
                      'message': 'External service failed to return a proper json'}
        }),
    )


async def service_failed_exception_handler(request: Request, exc: Exception):
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({
            'error': {'code': 'external_service',
                      'message': 'External service failed'}
        }),
    )
