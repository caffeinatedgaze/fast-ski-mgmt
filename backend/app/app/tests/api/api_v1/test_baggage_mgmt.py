import pytest
import httpx

from fastapi.testclient import TestClient

from app.main import app
from app.core.config import settings

from .data import anscillaries_pricings

client = TestClient(app)


def test_success(httpx_mock):
    """
    Check that response is valid when both external services respond properly
    """
    httpx_mock.reset(False)
    httpx_mock.add_response(url=settings.API_HOST + '/orders/?order_id=1234-1234-1234-1234&surname=Van+Rossum',
                            json=anscillaries_pricings)
    httpx_mock.add_response(url=settings.API_HOST + '/bags/', json={'shoppingCart': {}})

    response = client.post('http://localhost/api/v1/inventory/baggage-management/skis/',
                           json={'surname': 'Van Rossum', 'order_id': '1234-1234-1234-1234'})

    assert 200 == response.status_code
    assert 'error' not in response.json()


def test_first_fails(httpx_mock):
    """
    Check that response is correct if the first external service fails to respond properly
    """
    httpx_mock.reset(False)
    httpx_mock.add_response(url=settings.API_HOST + '/orders/?order_id=1234-1234-1234-1234&surname=Van+Rossum')

    response = client.post('http://localhost/api/v1/inventory/baggage-management/skis/',
                           json={'surname': 'Van Rossum', 'order_id': '1234-1234-1234-1234'})

    assert 200 == response.status_code
    assert 'error' in response.json(), 'Expected to get error due to external service malfunctioning'


def test_second_fails(httpx_mock):
    """
    Check that response is correct if the second external service fails to respond properly
    """
    httpx_mock.reset(False)
    httpx_mock.add_response(url=settings.API_HOST + '/orders/?order_id=1234-1234-1234-1234&surname=Van+Rossum',
                            json=anscillaries_pricings)
    httpx_mock.add_response(url=settings.API_HOST + '/bags/')

    response = client.post('http://localhost/api/v1/inventory/baggage-management/skis/',
                           json={'surname': 'Van Rossum', 'order_id': '1234-1234-1234-1234'})

    assert 200 == response.status_code
    assert 'error' in response.json(), 'Expected to get error due to external service malfunctioning'
